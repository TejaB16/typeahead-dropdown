import React, { useState } from 'react';
import { Grid, Button } from '@material-ui/core';
import ProgrammingLanguages from './components/ProgrammingLanguages';
import Companies from './components/Companies';
import Locations from './components/Locations';
import Roles from './components/Role';
import { roleOptions, ProgrammingLanguagesOptions, companiesOptions, locationOptions } from './constants'
import './App.css';

function Home() {
    const [value, setValue] = useState({
        role: '',
        programmingLanguages: [],
        companies: [],
        locations: []
    });
    const [isSubmitted, setIsSubmitted] = useState(false);
    return (
        <div className="App">
            <Grid container spacing={3} style={{ marginBottom: '12px' }}>
                <Grid item xs={12} className="app-heading">
                    Re-usable components demo
                </Grid>
            </Grid>
            <Grid container justify="space-between" spacing={3}>
                <Grid item xs={12} sm={6} lg={4}>
                    <Roles
                        options={roleOptions}
                        value={value}
                        setValue={setValue}
                    />
                    <ProgrammingLanguages
                        options={ProgrammingLanguagesOptions}
                        value={value}
                        setValue={setValue}
                    />
                    <Companies
                        options={companiesOptions}
                        value={value}
                        setValue={setValue}
                    />
                    <Locations
                        options={locationOptions}
                        value={value}
                        setValue={setValue}
                    />
                </Grid>
                {isSubmitted && window.innerWidth > 768 &&
                    <Grid item xs={12} sm={6} lg={4} className="resultBox">
                        {JSON.stringify(value)}
                    </Grid>
                }
            </Grid>

            <Grid item xs={12} sm={6} lg={1} style={{ marginTop: '12px', marginBottom: '24px' }}>
                <Button
                    fullWidth
                    variant="contained"
                    onClick={() => setIsSubmitted(true)}
                    style={{ backgroundColor: '#34568B', color: '#fff' }}
                >
                    Submit
                    </Button>
            </Grid>
            {isSubmitted && window.innerWidth < 769 &&
                <Grid item xs={12} sm={6} lg={4} className="resultBox">
                    {JSON.stringify(value)}
                </Grid>
            }
        </div>
    );
}

export default Home;
