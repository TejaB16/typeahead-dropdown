import React, { Component } from 'react';
import { TypeaheadDropDown } from '../TypeaheadDropdown/index';
import '../../App.css';

export default class Companies extends Component {

    render() {
        const { options, value, setValue } = this.props;
        return (
            <TypeaheadDropDown
                options={options}
                isMulti={true}
                color="#B565A7"
                headingLabel="Please select companies"
                onChange={selectedValue => setValue({...value, companies: selectedValue})}
            />
        )
    }
}
