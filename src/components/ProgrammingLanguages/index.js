import React, { Component } from 'react';
import { TypeaheadDropDown } from '../TypeaheadDropdown/index';
import '../../App.css';

export default class ProgrammingLanguages extends Component {
    render() {
        const { options, value, setValue } = this.props;
        return (
            <TypeaheadDropDown
                options={options}
                isMulti={true}
                color="#34568B"
                headingLabel="Please select programming languages"
                onChange={selectedValue => setValue({...value, programmingLanguages: selectedValue})}
            />
        )
    }
}
