import React, { Component } from 'react';
import { TypeaheadDropDown } from '../TypeaheadDropdown/index';
import '../../App.css';

export default class Roles extends Component {
    render() {
        const { options, value, setValue } = this.props;
        return (
            <TypeaheadDropDown
                options={options}
                headingLabel="Please select a role"
                onChange={selectedValue => setValue({...value, role: selectedValue})}
            />
        )
    }
}
