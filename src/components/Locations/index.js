import React, { Component } from 'react';
import { TypeaheadDropDown } from '../TypeaheadDropdown/index';
import '../../App.css';

export default class Locations extends Component {
    render() {
        const { options, value, setValue } = this.props;
        return (
            <TypeaheadDropDown
                options={options}
                isMulti={true}
                color="#955251"
                headingLabel="Please select locations"
                onChange={selectedValue => setValue({...value, locations: selectedValue})}
            />
        )
    }
}
