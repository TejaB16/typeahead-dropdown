import React, { Component, Fragment } from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

export class TypeaheadDropDown extends Component {
  animatedComponents = makeAnimated();
  CustomOption = props => {
    const { color } = this.props;
    const { innerProps, innerRef, data: { label, image } } = props;
    const optionBackground = `.custom-option:hover { background-color: ${color ? color : '#F2F3F4'}; color: ${color ? '#fff' : '#424949'};} .custom-option { color: ${color ? color : '#424949'} }`;
    return (
      <Fragment>
        <style>
          {optionBackground}
        </style>
        <div ref={innerRef} {...innerProps} className="custom-option">
          <div>{label}</div>
          {image && <img src={image} width={24} height={24} alt="program identity" />}
        </div>
      </Fragment>
    );
  };
  render() {
    const { options, isMulti, headingLabel, onChange } = this.props;
    return (
      <Fragment>
        { headingLabel &&
          <div className="heading">
            {headingLabel}
          </div>
        }
        <Select
          components={{ ...this.animatedComponents, Option: this.CustomOption }}
          options={options}
          isMulti={isMulti}
          onChange={onChange}
        />
      </Fragment>
    )
  }
}
