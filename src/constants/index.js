export const roleOptions = [
    { value: 'developer', label: 'Developer' },
    { value: 'qa', label: 'QA' },
];

export const ProgrammingLanguagesOptions = [
    { value: 'java', label: 'Java', image: 'images/java.png' },
    { value: 'python', label: 'Python', image: 'images/python.png' },
    { value: 'angular', label: 'Angular', image: 'images/angular.png' },
    { value: 'javascript', label: 'Javascript', image: 'images/javascript.svg' },
    { value: 'react', label: 'React', image: 'images/react.png' },
    { value: 'reactnative', label: 'React Native', image: 'images/react.png' },
    { value: 'nodejs', label: 'Node js', image: 'images/nodejs.jpg' },
];

export const companiesOptions = [
    { value: 'tcs', label: 'TCS' },
    { value: 'infosys', label: 'Infosys' },
    { value: 'capgemini', label: 'Capgemini' },
    { value: 'accenture', label: 'Accenture' },
    { value: 'wipro', label: 'Wipro' },
    { value: 'pharmeasy', label: 'Pharmeasy' },
    { value: 'superwoman', label: 'Super woman' },
];

export const locationOptions = [
    { value: 'hyderabad', label: 'Hyderabad' },
    { value: 'bangalore', label: 'Bangalore' },
    { value: 'chennai', label: 'Chennai' },
    { value: 'mumbai', label: 'Mumbai' },
    { value: 'pune', label: 'Pune' },
    { value: 'trichy', label: 'Trichy' },
    { value: 'delhi', label: 'Delhi' },
];